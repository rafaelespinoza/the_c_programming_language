// Exercise 3-2
// Write a function `escape(dest, source)` that converts characters like newline
// and tab into visible escape character sequences like `\n` and `\t` as it
// copies the string `source` into `dest`. Write a function for the other
// direction as well, converting escape sequences to the real characters.

#include <stdio.h>

void escape(char dest[], char source[]);
void unescape(char dest[], char source[]);

int main () {
	char source[60] = "\aHello,\n\tWorld! The mistakee\b is \van \"Extra 'e'\"!\n";
	char dest[61];

	printf("Original:\n%s\n\n", source);

	escape(dest, source);
	printf("Escaped:\n%s\n\n", dest);

	unescape(source, dest);
	printf("Unescaped:\n%s\n", source);

	return 0;
}

void escape(char dest[], char source[]) {
	char ch;
	unsigned int i, j;

	i = j = 0;

	while ((ch = source[j++]) != '\0') {
		switch (ch) {
			case '\a':
				dest[i++] = '\\';
				dest[i++] = 'a';
				break;
			case '\b':
				dest[i++] = '\\';
				dest[i++] = 'b';
				break;
			case '\f':
				dest[i++] = '\\';
				dest[i++] = 'f';
				break;
			case '\n':
				dest[i++] = '\\';
				dest[i++] = 'n';
				break;
			case '\t':
				dest[i++] = '\\';
				dest[i++] = 't';
				break;
			case '\r':
				dest[i++] = '\\';
				dest[i++] = 'r';
				break;
			case '\v':
				dest[i++] = '\\';
				dest[i++] = 'v';
				break;
			case '\\':
				dest[i++] = '\\';
				dest[i++] = '\\';
				break;
			case '\"':
				dest[i++] = '\\';
				dest[i++] = '\"';
				break;
			default:
				dest[i++] = ch;
				break;
		}
	}
	dest[i] = '\0';
}

void unescape(char dest[], char source[]) {
	unsigned int i, j;

	i = j = 0;

	while (source[j] != '\0') {
		if (source[j] != '\\') {
			dest[i] = source[j];
			i++;
			j++;
			continue;
		}
		// check escaped character
		j++;
		switch (source[j]) {
			case 'a':
				dest[i] = '\a';
				break;
			case 'b':
				dest[i] = '\b';
				break;
			case 'f':
				dest[i] = '\f';
				break;
			case 'n':
				dest[i] = '\n';
				break;
			case 't':
				dest[i] = '\t';
				break;
			case 'r':
				dest[i] = '\r';
				break;
			case 'v':
				dest[i] = '\v';
				break;
			case '\\':
				dest[i] = '\\';
				break;
			case '\"':
				dest[i] = '\"';
				break;
			default:
				i++;
				dest[i] = '\\';
				dest[i] = source[j];
				break;
		}
		i++;
		j++;
	}
	dest[i] = '\0';
}
