// Exercise 4-1
// Write a function which returns the position of the *rightmost* occurrence of
// a character in a string. If it doesn't occur, return -1;

#include <stdio.h>

int strrindex(char string[], char *target);

int main () {
	char strings[5][20] = {
		"yabba dabba doo",
		"\rvan\v\thalen",
		"a b c",
		"9876543210",
		"\b"
	};
	char targets[5][1] = {
		"a",
		"\t",
		"d",
		"7",
		"c"
	};
	int i, result;

	for (i = 0; i < 5; i++) {
		result = strrindex(strings[i], targets[i]);
		printf("%20s, %c, %3d\n", strings[i], *targets[i], result);
	}
}

int strrindex(char string[], char *target) {
	int i, match;
	match = -1;

	for (i = 0; string[i] != '\0'; i++) {
		if (string[i] == *target && i > match) {
			match = i;
		}
	}

	return match;
}
