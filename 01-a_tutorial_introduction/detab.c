// Exercise 1-20
// Write a program called detab that replaces tabs in the input with the proper
// number of blanks to space to the next tab stop. Assume a fixed set of tab
// stops (every n columns).

#include <ctype.h>
#include <stdio.h>

void detab(int num_tab_stops) {
	int ch, i, j;
	i = 0;
	while ((ch = getchar()) != EOF) {
		if (ch != '\t') {
			putchar(ch);
			i++;
			continue;
		}
		j = i;
		while (j % num_tab_stops != 0) {
			// for visibility & testing, output a dot, not a space.
			putchar('.');
			j++;
		}
		i++;
	}
	putchar('\n');
}

int main () {
	detab(4);
}
