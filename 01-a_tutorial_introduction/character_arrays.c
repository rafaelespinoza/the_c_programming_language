// 1.9 Character Arrays

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "defs.h"

#define LENGTH_LIMIT 1000
#define EIGHTY 80

// print_longest_line is the example code from the chapter.
void print_longest_line () {
	int i, curr_line_length, max_line_length;
	char longest_line[LENGTH_LIMIT];
	char current_line[LENGTH_LIMIT];

	curr_line_length = max_line_length = 0;
	for (i = 0; i < LENGTH_LIMIT; i++) {
		longest_line[i] = ' ';
	}

	while ((curr_line_length = get_line(current_line, LENGTH_LIMIT)) > 0) {
		if (curr_line_length > max_line_length) {
			max_line_length = curr_line_length;
			copy_line(longest_line, LENGTH_LIMIT, current_line);
		}
	}
	printf("max_line_length %d\n", max_line_length);
	if (max_line_length > 0) {
		printf("%s\n", longest_line);
	}
}

// Exercise 1-17
// Write a program to print input lines that are longer than 80 characters.
void print_longer_than_80_chars () {
	int curr_line_length;
	char current_line[LENGTH_LIMIT];

	curr_line_length = 0;

	while ((curr_line_length = get_line(current_line, LENGTH_LIMIT)) > 0) {
		if (curr_line_length > EIGHTY) {
			printf("%s", current_line);
		}
	}
}

// Exercise 1-18
// Write a program to remove trailing blanks and tabs from each line of input,
// and to entirely delete blank lines.
void remove_trailing_blanks_and_blank_lines () {
	int curr_line_length, i, output_length;
	char current_line[LENGTH_LIMIT];
	char output_line[LENGTH_LIMIT];

	curr_line_length = 0;
	for (i = 0; i < LENGTH_LIMIT; i++) {
		current_line[i] = ' ';
	}

	while ((curr_line_length = get_line(current_line, LENGTH_LIMIT)) > 0) {
		if (curr_line_length == 1) {
			// Instead of deleting blank lines like the exercise
			// says, we're just not printing them. This is in the
			// interest of idempotency. Length 1 means it's only the
			// new line character.
			continue;
		}
		output_length = rm_trailing_whitespace(
					output_line,
					curr_line_length,
					current_line
				);
		for (i = 0; i < output_length; i++) {
			putchar(output_line[i]);
		}
		// visibly mark end of line for easy manual testing
		putchar('|');
		putchar('\n');
	}
}

// Exercise 1-19
// Write a program that reverses its input a line at a time.
void reverse_characters_in_each_line () {
	int curr_line_length, i;
	char current_line[LENGTH_LIMIT];

	curr_line_length = 0;
	for (i = 0; i < LENGTH_LIMIT; i++) {
		current_line[i] = ' ';
	}

	while ((curr_line_length = get_line(current_line, LENGTH_LIMIT)) > 0) {
		for (i = curr_line_length; i >= 0; i--) {
			putchar(current_line[i]);
		}
	}
	putchar('\n');
}

int main () {
	// NOTE: Uncomment one of these at a time.
	// print_longest_line();
	// print_longer_than_80_chars();
	// remove_trailing_blanks_and_blank_lines();
	// reverse_characters_in_each_line();
}
