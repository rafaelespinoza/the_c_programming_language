#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

// get_line reads a line of input into buffer and returns its length. If it
// reaches the end of the file, then it returns 0.
int get_line(char buffer[], int max_line) {
	int ch, i;
	i = 0;
	while (i < max_line - 1) {
		ch = getchar();
		if (ch == EOF || ch == '\n') {
			break;
		}
		buffer[i] = ch;
		i++;
	}
	if (ch == '\n') {
		buffer[i] = ch;
		i++;
	}
	buffer[i] = '\0';
	return i;
}

// copy_line duplicates values from src into dest. It breaks early if it detects
// a null terminator. It also assumes that both arrays are the same length.
void copy_line (char dest[], int limit, char src[]) {
	for (int i = 0; i < limit; i++) {
		dest[i] = src[i];
		if (src[i] == '\0') {
			break;
		}
	}
}

// Stores the trimmed input string into the given output buffer, which must be
// large enough to store the result. If it is too small, the output is
// truncated. Based on https://stackoverflow.com/a/122721
size_t rm_trailing_whitespace (char *output_buffer, size_t len, const char *input) {
	if (len == 0) {
		return 0;
	} else if (*input == 0) { // All spaces?
		*output_buffer = 0;
		return 1;
	}

	const char *end; // pointer to a constant character
	size_t out_size;

	// Trim trailing space
	end = input + strlen(input) - 1;
	while (end > input && isspace((unsigned char)*end)) { end--; }
	end++;

	out_size = (end - input) < len-1
		? (end - input)
		: len - 1;

	memcpy(output_buffer, input, out_size);
	output_buffer[out_size] = 0;

	return out_size;
}
