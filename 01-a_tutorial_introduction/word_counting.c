// 1.5.4 Word Counting

#include <stdio.h>

#define IN 1
#define OUT 0

void example () {
	int ch, line_count, word_count, char_count, state;
	state = OUT;
	line_count = word_count = char_count = 0;

	while ((ch = getchar()) != EOF) {
		++char_count;
		if (ch == '\n' || ch == '\r') {
			++line_count;
		}
		if (ch == ' ' || ch == '\n' || ch == '\t') {
			state = OUT;
		} else if (state == OUT) {
			state = IN;
			++word_count;
		}
	}

	printf("line_count: %3d, word_count: %3d, char_count: %3d\n",
			line_count, word_count, char_count);
}

// Exercise 1-12
// write a program the prints its input one word per line.
void print_one_word_per_line () {
	int ch;

	while ((ch = getchar()) != EOF) {
		if (ch == '\n' || ch == '\r' || ch == '\t' || ch == ' ') {
			putchar('\n');
		} else {
			putchar(ch);
		}
	}
}

int main () {
	// NOTE: Uncomment one of these at a time.
	// example();
	// print_one_word_per_line();
}
