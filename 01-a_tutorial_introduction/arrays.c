// 1.6 Arrays

#include <stdio.h>
#include <ctype.h>
#include "defs.h"

#define WORD_LENGTH_LIMIT 20

void example () {
	int ch, i, whitespace_count, other_count;
	int ndigit[10];

	whitespace_count = other_count = 0;
	for (i = 0; i < 10; i++) {
		ndigit[i] = 0;
	}

	while ((ch = getchar()) != EOF) {
		if (ch >= '0' && ch <= '9') {
			ndigit[ch-'0']++;
		} else if (ch == ' ' || ch == '\n' || ch == '\t') {
			whitespace_count++;
		} else {
			other_count++;
		}
	}

	printf("digits =");
	for (i = 0; i < 10; i++) {
		printf(" %d", ndigit[i]);
	}
	printf(", white space = %d, other = %d\n",
			whitespace_count, other_count);
}

// Exercise 1-13
// Write a program to print a histogram of the lengths of words of its input.
// First, try to print bars horizontally, then try vertically.
void print_word_length_histogram () {
	int ch, i, curr_word_len, max_word_len, is_in_word;
	int word_lengths[WORD_LENGTH_LIMIT];

	curr_word_len = max_word_len = 0;
	is_in_word = FALSE;
	for (i = 0; i < WORD_LENGTH_LIMIT; i++) {
		word_lengths[i] = 0;
	}

	while ((ch = getchar()) != EOF) {
		if (is_in_word == TRUE && (isalpha(ch) || ch == '_')) {
			curr_word_len++;
			continue;
		}
		if (is_in_word == FALSE) {
			is_in_word = TRUE;
			curr_word_len++;
			continue;
		}
		if (is_in_word == TRUE && curr_word_len >= 1 && curr_word_len < WORD_LENGTH_LIMIT) {
			word_lengths[curr_word_len]++;
			if (word_lengths[curr_word_len] > max_word_len) {
				max_word_len = word_lengths[curr_word_len];
			}
		}
		curr_word_len = 0;
		is_in_word = FALSE;
	}

	// show results
	int y = 0;
	int x = 0;

	// horizontal histogram
	for (y = 0; y < WORD_LENGTH_LIMIT; y++) {
		printf("%2d ", y);
		for (x = 0; x < word_lengths[y]; x++) {
			printf("|");
		}
		printf("\n");
	}

	// vertical histogram
	// counts
	for (y = max_word_len; y >= 0; y--) {
		for (x = 0; x < WORD_LENGTH_LIMIT; x++) {
			if (word_lengths[x] > y) {
				printf("  *");
			} else {
				printf("   ");
			}
		}
		printf("\n");
	}
	// vertical line
	for (x = 0; x < WORD_LENGTH_LIMIT; x++) {
		printf("---");
	}
	printf("\n");
	// word lengths (x-axis)
	for (x = 0; x < WORD_LENGTH_LIMIT; x++) {
		printf("%3d", x);
	}
	printf("\n");
}

// Exercise 1-14
// Write a program to print a histogram of the frequencies of different
// characters in its input.
void print_character_histogram () {
	int ch, i;
	int char_counts[26];

	for (i = 0; i < 26; i++) {
		char_counts[i] = 0;
	}

	while ((ch = getchar()) != EOF) {
		if (islower(ch)) {
			char_counts[ch - 97]++;
		} else if (isupper(ch)) {
			char_counts[ch - 65]++;
		}
	}

	// show results
	int y, x;
	char letter;

	// horizontal histogram
	for (y = 0; y < 26; y++) {
		letter = y+65;
		printf("%2c ", letter);
		for (x = 0; x < char_counts[y]; x++) {
			printf("|");
		}
		printf("\n");
	}
}

int main () {
	// NOTE: Uncomment one of these at a time.
	// example();
	// print_word_length_histogram();
	// print_character_histogram();
}
