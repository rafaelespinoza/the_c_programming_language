// 1.5.3 Line Counting

#include <stdio.h>

// Exercise 1-8
// write a program to count blanks, tabs, newlines
void count_blanks_tabs_newlines () {
	int ch, blank_count, tab_count, line_count, state;
	blank_count = tab_count = line_count = 0;

	while ((ch = getchar()) != EOF) {
		if (ch == '\n' || ch == '\r') {
			++line_count;
		} else if (ch == ' ') {
			++blank_count;
		} else if (ch == '\t') {
			++tab_count;
		}
	}

	printf("blank_count: %3d, tab_count: %3d, line_count: %3d\n",
			blank_count, tab_count, line_count);
}

// Exercise 1-9
// write a program to copy its input to output, replacing each string of one or
// more blanks by a single blank.
void copy_and_replace_blanks () {
	int ch;
	while ((ch = getchar()) != EOF) {
		if (ch == ' ') {
			while ((ch = getchar()) == ' '); // iterate
			putchar(' ');
			if (ch == EOF) {
				break;
			}
		}
		putchar(ch);
	}
}

// Exercise 1-10
// write a program to copy its input to output, replacing each tab by `\t`, each
// backspace by `\b` and each backslash by `\\`. This makes tabs and backspaces
// visible in an umambiguous way.
void copy_and_replace_tabs_backspaces () {
	int ch;
	while ((ch = getchar()) != EOF) {
		// NOTE: putchar does not accept string constants such as "\\"
		// or "\\t". It only accepts character constants, ie: '\\', 't'
		if (ch == '\t') {
		        putchar('\\');
		        putchar('t');
		} else if (ch == '\b') {
		        putchar('\\');
		        putchar('b');
		} else if (ch == '\\') {
		        putchar('\\');
		        putchar('\\');
		} else {
		        putchar(ch);
		}
	}
}

int main () {
	// NOTE: Uncomment one of these at a time.
	// count_blanks_tabs_newlines();
	// copy_and_replace_blanks();
	// copy_and_replace_tabs_backspaces();
}
