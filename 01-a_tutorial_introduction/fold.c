// Exercise 1-22
// Write a program to "fold" long input lines into two or more shorter lines after the last non-blank character that occurs before the n-th column of the input. Make sure your program does something intelligent w/ very long lines and if there are no blanks or tabs before the specified column.

#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>

#define TAB_WIDTH 8
#define WORD_LENGTH_LIMIT 1023

// adjust_position helps you figure out which column to print the next char on.
size_t adjust_position (size_t pos, char ch, int tab_width);
// fold is my first go at implementing fold.
void fold(int width);
// gnu_fold is a simplified refactor of the actual "fold" command from GNU
// coreutils. https://github.com/coreutils/coreutils/blob/master/src/fold.c
void gnu_fold(int width);
// print_buffer outputs the currently saved word.
void print_buffer(char buffer[], int length);
void shift_chars_left (char src[], int start_ind, int num);

int main () {
	gnu_fold(9);
}

void fold(int width) {
	int ch, pos, last_space, curr_word_len;
	bool was_in_word;
	char curr_word[WORD_LENGTH_LIMIT];
	curr_word_len = 0;
	pos = last_space = -1;

	while ((ch = getchar()) != EOF) {
		pos++;
		if (ch == '\n') {
			if (pos > width) {
				putchar(ch);
			}
			print_buffer(curr_word, curr_word_len);
			putchar(ch);
			pos = last_space = -1;
			curr_word_len = 0;
			was_in_word = false;
			continue;
		}
		if ((ch == '\t' || ch == ' ')) {
			if (was_in_word) {
				was_in_word = false;
				if (pos > width) {
					putchar('\n');
					print_buffer(curr_word, curr_word_len);
					pos = curr_word_len;
					last_space = -1;
					putchar('.');
				} else {
					print_buffer(curr_word, curr_word_len);
					last_space = pos;
				}
				curr_word_len = 0;
			}
			continue;
		}
		if (curr_word_len >= WORD_LENGTH_LIMIT) {
			// TODO: handle long words
			continue;
		}
		if (!was_in_word) {
			was_in_word = true;
			while (last_space >= 0 && last_space < pos) {
				putchar('.');
				last_space++;
			}
		}
		curr_word[curr_word_len] = ch;
		curr_word_len++;
	}

	if (curr_word_len > 0) {
		print_buffer(curr_word, curr_word_len);
	}
}

void gnu_fold(int width) {
	int ch = 0;
	// next_pos is the position in the line where next character would go.
	size_t next_pos = 0;
	// line_len is the current length of the current line.
	size_t line_len = 0;
	// line is a buffer to store current line.
	char line[WORD_LENGTH_LIMIT];
	// pause tells us if we want to rescan the same character. We'd want to
	// do this just after folding the line so we can print the character
	// that would have been at the width limit had we not folded the line.
	// Using a variable is better than using a `goto` statement, which is
	// what the GNU coreutils version does.
	bool pause = false;

	while (ch != EOF) {
		if (!pause) {
			ch = getchar();
		}
		if (!pause && ch == '\n') {
			line[line_len] = ch;
			line_len++;
			print_buffer(line, line_len);
			next_pos = line_len = 0;
			pause = false;
			continue;
		} else {
			pause = false;
			next_pos = adjust_position(next_pos, ch, TAB_WIDTH);
			if (next_pos > width) {
				// Fold here by printing a newline and the
				// buffer contents.
				bool found_blank = false;
				size_t logical_end = next_pos;
				// look for last blank
				while (logical_end > 0) {
					logical_end--;
					if (isblank((char) line[logical_end])) {
						found_blank = true;
						break;
					}
				}
				if (found_blank) {
					size_t i;
					// Found a blank. Do not output anything
					// after it.
					logical_end++;
					print_buffer(line, logical_end);
					putchar('\n');

					// Move remainder to beginning of the
					// next line. The area being copied here
					// might overlap.
					shift_chars_left(line, logical_end, line_len - logical_end);

					line_len -= logical_end;
					for (next_pos = i = 0; i < line_len; i++) {
						next_pos = adjust_position(next_pos, ch, TAB_WIDTH);
					}
					pause = true;
					continue;
				}

				if (next_pos == 0) {
					line[line_len] = ch;
					line_len++;
					pause = false;
					continue;
				}

				line[line_len] = '\n';
				line_len++;
				print_buffer(line, line_len);
				next_pos = line_len = 0;
				pause = true;
				continue;
			}
			line[line_len] = ch;
			line_len++;
		}
	}
}

void print_buffer(char buffer[], int length) {
	int i;
	for (i = 0; i < length; i++) {
		putchar(buffer[i]);
	}
}

size_t adjust_position (size_t pos, char ch,  int tab_width) {
	switch (ch) {
		case '\b': return pos > 0 ? pos - 1  : pos;
		case '\r': return 0;
		case '\t': return pos + tab_width - pos % tab_width;
		default: return pos + 1;
	}
}

void shift_chars_left (char src[], int start, int num) {
	int i, stop;
	stop = start + num;
	for (i = 0; i < stop; i++) {
		src[i] = src[i + start];
		if (src[i] == '\0') {
			break;
		}
	}
}
