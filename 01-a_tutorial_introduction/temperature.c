// 1.2 Variables and Arithmetic Expressions

#include <stdio.h>

#define LOWER 0
#define UPPER 300
#define STEP  20

// print a Fahrenheit-Celsius table
void f_to_c () {
	float fahr, celsius;

	fahr = LOWER;
	printf("%4s %6s\n", "F", "C");
	printf("%4s %6s\n", "---", "---");
	while (fahr <= UPPER) {
		celsius = (5.0/9.0) * (fahr-32.0);
		printf("%4.0f %6.1f\n", fahr, celsius);
		fahr += STEP;
	}
}

// Exercise 1-4
// print a Celsius-Fahrenheit table
void c_to_f () {
	float fahr, celsius;

	celsius = LOWER;
	printf("%6s %4s\n", "C", "F");
	printf("%6s %4s\n", "---", "---");
	while (celsius <= UPPER) {
		fahr = 32 + celsius * 9.0 / 5.0;
		printf("%6.1f %4.0f\n", celsius, fahr);
		celsius += STEP;
	}
}

int main () {
	f_to_c();
	c_to_f();
}
