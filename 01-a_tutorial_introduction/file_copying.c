// 1.5.1 File Copying

#include <stdio.h>

int main () {
	int ch;
	ch = getchar();
	while (ch != EOF) {
		putchar(ch);
		ch = getchar();
	}
	// Exercise 1-6
	// verify that expression getchar() != EOF is 0 or 1
	int ch_not_equal_to_EOF = ch != EOF;
	if (ch_not_equal_to_EOF == 0) {
		printf("ch != EOF is equal to 0\n");
	} else if (ch != EOF == 1) {
		printf("ch != EOF is equal to 1\n");
	} else {
		printf("woah, it's %d\n", ch_not_equal_to_EOF);
	}

	// Exercise 1-7
	// write a program to print the value of EOF
	printf("EOF: %d\n", EOF);
}
