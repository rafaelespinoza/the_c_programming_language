// Exercise 2-3
// Write a function which converts a string of hexadecimal digits (including the
// optional 0x or 0X) into its equivalent integer value. The allowable digits
// are 0-9, A-F and a-f.

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#define BASE 16
// DIST_0 is distance from ascii '0' to 0..
#define DIST_0 48
// DIST_UPPER_A is distance from ascii 'A' to 10.
#define DIST_UPPER_A 55
// DIST_LOWER_A is distance from ascii 'a' to 10.
#define DIST_LOWER_A 87

unsigned int htoi (char input[]);

int main () {
	int i;
	unsigned int out;
	char *inputs[15] = {
		"1ab",
		"1AB",
		"0x1ab",
		"0x1AB",
		"0X1ab",
		"0X1AB",
		"F00",
		"bar",
		"0100",
		"0x0C0BE",
		"abcdef",
		"123456",
		"0x123456",
		"DEADBEEF",
		"zyx_wvu"
	};
	for (i = 0; i < 15; i++) {
		out = htoi(inputs[i]);
		if (out > 0) {
			printf("%10s: %15u\n", inputs[i], out);
		}
	}
	return 0;
}

unsigned int htoi (char input[]) {
	int i, dec, power;
	unsigned int sum;
	size_t last_ind
	char ch;

	i = dec = power = 0;
	sum = 0;
	last_ind = strlen(input) - 1;

	for (i = last_ind; i >= 0; i--) {
		power = last_ind - i;
		ch = input[i];
		if (isdigit(ch)) {
			dec = (ch - DIST_0);
		} else if (ch >= 'A' && ch <= 'F') {
			dec = (ch - DIST_UPPER_A);
		} else if (ch >= 'a' && ch <= 'f') {
			dec = (ch - DIST_LOWER_A);
		} else if (i > 0 && (ch == 'X' || ch == 'x')) {
			// Assume we're at the optional `0X` or `0x` prefix.
			break;
		} else {
			printf("ERROR: invalid input; index: %d, char: \"%c\", input: \"%s\"\n", i, ch, input);
			return 0;
		}
		sum += dec * pow(BASE, power);
	}
	return sum;
}
