// Exercise 2-4
// Write a function `squeeze` that deletes each character in a string that
// matches *any* character in another string.

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define MAX_CHAR_CODE 128

// squeeze removes any character in src that matches any character in rem.
void squeeze(char src[], char rem[]);
// squeeze_copy is variation of squeeze which does not attempt to modify src.
// Instead it copies characters into out and returns the number of characters
// copied into it.
int squeeze_copy(char out[], char src[], char rem[]);

int main () {
	char src[] = "abcdef";
	char rem[] = "ace";
	printf("\tsrc: %30s\n", src);
	printf("\trem: %30s\n", rem);
	squeeze(src, rem);
	printf("\tout: %30s\n", src);

	int out2_len;
	char src2[] = "abcdef";
	char rem2[] = "ace";
	char out2[strlen(src2)];
	printf("\tsrc: %30s\n", src2);
	printf("\trem: %30s\n", rem2);
	out2_len = squeeze_copy(out2, src2, rem2);
	printf("\tout: %30s, length: %d\n", out2, out2_len);

	return 0;
}

void squeeze(char src[], char rem[]) {
	int i, j;
	size_t rem_length;
	bool char_presence[MAX_CHAR_CODE];
	rem_length = strlen(rem);
	for (i = 0; i < MAX_CHAR_CODE; i++) {
		char_presence[i] = false;
	}
	for (i = 0; i < rem_length; i++) {
		if (rem[i] < MAX_CHAR_CODE) {
			char_presence[rem[i] - 0] = true;
		}
	}
	for (i = j = 0; src[i] != '\0'; i++) {
		if (!char_presence[src[i] - 0]) {
			src[j] = src[i];
			j++;
		}
	}
	src[j] = '\0';
}

int squeeze_copy(char out[], char src[], char rem[]) {
	int i, j;
	size_t rem_length;
	bool char_presence[MAX_CHAR_CODE];
	rem_length = strlen(rem);
	for (i = 0; i < MAX_CHAR_CODE; i++) {
		char_presence[i] = false;
	}
	for (i = 0; i < rem_length; i++) {
		if (rem[i] < MAX_CHAR_CODE) {
			char_presence[rem[i] - 0] = true;
		}
	}
	for (i = j = 0; src[i] != '\0'; i++) {
		if (!char_presence[src[i] - 0]) {
			out[j] = src[i];
			j++;
		}
	}
	out[j] = '\0';
	return j;
}
