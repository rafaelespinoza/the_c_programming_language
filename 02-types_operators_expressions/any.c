// Exercise 2-5
// Write a function `any` which returns the first location in a string where any
// character from another another string occurs. If the other string contains no
// characters in the first string, then return -1.

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define MAX_CHAR_CODE 128

int any(char src[], char rem[]);

int main () {
	char src1[] = "abcdef";
	char rem1[] = "123456";
	int out1;
	printf("\tsrc: %30s\n", src1);
	printf("\trem: %30s\n", rem1);
	out1 = any(src1, rem1);
	printf("\tout: %30d\n", out1);

	char src2[] = "abcdef";
	char rem2[] = "hgfedc";
	int out2;
	printf("\tsrc: %30s\n", src2);
	printf("\trem: %30s\n", rem2);
	out2 = any(src2, rem2);
	printf("\tout: %30d\n", out2);
}

int any(char src[], char rem[]) {
	int i, j;
	size_t rem_length;
	bool char_presence[MAX_CHAR_CODE];
	rem_length = strlen(rem);
	for (i = 0; i < MAX_CHAR_CODE; i++) {
		char_presence[i] = false;
	}
	for (i = 0; i < rem_length; i++) {
		if (rem[i] < MAX_CHAR_CODE) {
			char_presence[rem[i] - 0] = true;
		}
	}
	for (i = j = 0; src[i] != '\0'; i++) {
		if (char_presence[src[i] - 0]) {
			return i;
		}
	}
	return -1;
}
