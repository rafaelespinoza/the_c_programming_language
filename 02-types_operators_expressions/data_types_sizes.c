// 2.2 Data Types and Sizes

#include <limits.h>
#include <stdio.h>

void print_size (char* type, long lo, long hi) {
	printf("%-14s [%20ld, %20ld]\n", type, lo, hi);
}

// Exercise 2-1
// Write a program to determine the ranges of char, short, int, long variables,
// both signed and unsigned by printing appropriate values from standard headers
// and by direct computation.
int main () {
	// output header with horizontal line.
	printf("%-14s %-22s %-22s\n", "type", "lo", "hi");
	for (int i = 0; i <= 58; i++) {
		printf("-");
	}
	printf("\n");

	// output data
	print_size("signed char", CHAR_MIN, CHAR_MAX);
	print_size("signed int", INT_MIN, INT_MAX);
	print_size("signed long", LONG_MIN, LONG_MAX);
	print_size("signed short", SHRT_MIN, SHRT_MAX);

	print_size("unsigned char", 0, UCHAR_MAX);
	print_size("unsigned int", 0, UINT_MAX);
	print_size("unsigned long", 0, ULONG_MAX);
	print_size("unsigned short", 0, USHRT_MAX);
}
